create table usuario (
  id serial primary key,
  ci int,
  paterno varchar(20),
  materno varchar(20),
  nombre varchar(30),
  genero smallint,
  estado smallint,
  fnac date,
  createdAt timestamp
);