package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	_ "github.com/lib/pq"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "taylor"
	password = "tallarin3"
	dbname   = "simpleform"
)

type usuario struct {
	ID        int    `json:"id"`
	Ci        int    `json:"ci"`
	Paterno   string `json:"paterno"`
	Materno   string `json:"materno"`
	Nombre    string `json:"nombre"`
	Genero    int    `json:"genero"`
	Estado    int    `json:"estado"`
	Fnac      string `json:"fnac"`
	CreatedAt string `json:"createdAt"`
}
type response struct {
	Status   string      `json:"status"`
	Response interface{} `json:"reponse"`
}

func dbConn() (db *sql.DB) {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
	return db
}

func insert(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	db := dbConn()
	decoder := json.NewDecoder(r.Body)
	var user usuario
	err := decoder.Decode(&user)
	if err != nil {
		panic(err)
	}
	if r.Method == "POST" {
		insDb, err := db.Prepare("INSERT INTO USUARIO VALUES(default, $1, $2, $3, $4, $5, $6, $7, $8)")
		if err != nil {
			panic(err.Error())
		}
		insDb.Exec(user.Ci, user.Paterno, user.Materno, user.Nombre, user.Genero, user.Estado, user.Fnac, time.Now())
	}
	defer db.Close()
	res, _ := json.Marshal(response{"success", nil})
	w.Header().Set("Content-Type", "application/json")
	w.Write(res)
	fmt.Println(user)
}
func list(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	db := dbConn()
	selDb, err := db.Query("select * from usuario")
	if err != nil {
		panic(err.Error())
	}
	users := []usuario{}
	var user usuario
	for selDb.Next() {
		selDb.Scan(&user.ID, &user.Ci, &user.Paterno, &user.Materno, &user.Nombre, &user.Genero, &user.Estado, &user.Fnac, &user.CreatedAt)
		users = append(users, user)
	}
	defer db.Close()
	res, _ := json.Marshal(response{"success", users})
	w.Header().Set("Content-Type", "application/json")
	w.Write(res)
}
func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Access-Control-Allow-Methods", "GET, POST")
	(*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
}

func main() {
	fmt.Println("Serving at localhost:8081")
	http.HandleFunc("/insert", insert)
	http.HandleFunc("/list", list)
	http.ListenAndServe(":8081", nil)
}
